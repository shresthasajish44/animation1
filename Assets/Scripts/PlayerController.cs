using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerController : MonoBehaviour
{
    public float moveSpeed = 1;
    public float lookSens = 5;

    public Animator anim;
    public Controller inputActions;

    private Vector2 moveVector;
    private Vector2 lookVector;
    private Vector3 rotation;
    
    private CharacterController characterController;

    private float jumpHeight = 10f;
    private float gravity = -9.81f;
    private float verticalVelocity;

    // Start is called before the first frame update
    void Start()
    {
        characterController = GetComponent<CharacterController>();
    }

    // Update is called once per frame
    void Update()
    {
        Move();
        Rotate();
        Jump();
    }

    public void OnMove(InputAction.CallbackContext context)
    {
        moveVector = context.ReadValue<Vector2>();
    }

    private void Move() 
    {
        Vector3 move = transform.right * moveVector.x + transform.forward * moveVector.y;
        characterController.Move(move * moveSpeed * Time.deltaTime);
        if (moveVector.magnitude > 0)
        {
            anim.SetBool("isWalking", true);
        }
        else {
            anim.SetBool("isWalking", false);
        }
    }

    public void OnLook(InputAction.CallbackContext context) 
    {
        lookVector = context.ReadValue<Vector2>();
    }

    private void Rotate() {
        rotation.y += lookVector.x * lookSens * Time.deltaTime;
        transform.localEulerAngles = rotation;
    }


    public void OnJump(InputAction.CallbackContext context) 
    {
        if (characterController.isGrounded && context.performed) {
            anim.Play("Jump");
            Jump();
        }
    }

    private void Jump() {
        verticalVelocity = Mathf.Sqrt(jumpHeight * gravity);
    }
}
